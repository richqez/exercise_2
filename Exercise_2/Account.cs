﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_2
{
    public class AccountManageException : Exception
    {
        public AccountManageException(float value)
            : base(String.Format("[ERR] This accout balance less than 100 and current is {0}",value))
        {

        }
    }
    class Account
    {

        public Guid accountId { get;  private set; }
        public float accountBalance { get; private set; }

        public Account()
        {
            this.accountId = new Guid();
            this.accountBalance = 100;
        }

        public void deposit(float value)
        {
            if (isValidBalance())
            {
                this.accountBalance += value;
            }
        }

        public void withDraw(float value)
        {
            if (isValidBalance())
            {
                 this.accountBalance -= value;
            }
        }

        public float checkBalance()
        {
            return this.accountBalance;
        }

        private bool isValidBalance()
        {
            if (this.accountBalance >= 100)
            {
                return true;
            }
            else
            {
                throw new AccountManageException(this.accountBalance);
            }

        }



    }
}
