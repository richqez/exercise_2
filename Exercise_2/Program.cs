﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_2
{
    class Program
    {
        public static bool IsNumeric(object Expression)
        {
            double retNum;

            bool isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);
            return isNum;
        }

        public static void displayMenu(){
            Console.WriteLine("Account");
            Console.WriteLine("========");
            Console.WriteLine("1. Deposit");
            Console.WriteLine("2. Withdraw");
            Console.WriteLine("3. CheckBalance");
            Console.WriteLine("0. Exit");
        }

        public static void displayBalance(float value)
        {
            Console.WriteLine(String.Format("Account Balance is : {0}", value.ToString()));

        }

        static void Main(string[] args)
        {
            
            Account account = new Account();
            

            while (true)
            {
                displayMenu();
                var input = "";
                Console.Write("Select Number : ");
                string selected = Console.ReadLine();
                switch (selected)
                {
                    case "1":
                        Console.Write("Enter money to deposit : ");
                        input = Console.ReadLine();
                        if (IsNumeric(input))
                        {
                            try
                            {
                                account.deposit(float.Parse(input));
                                displayBalance(account.checkBalance());
                            }
                            catch(AccountManageException ex){
                                Console.WriteLine(ex.Message);
                                continue;
                            }
                        }
                        else
                        {
                            Console.WriteLine("[ERR] input is not a number");
                        }
                        continue;    
                    case "2":
                        Console.Write("Enter money to withdraw : ");
                        input = Console.ReadLine();
                        if (IsNumeric(input))
                        {
                            try
                            {
                                account.withDraw(float.Parse(input));
                                displayBalance(account.checkBalance());
                            }
                            catch(AccountManageException ex){
                                Console.WriteLine(ex.Message);
                                continue;
                            }
                        }
                        else
                        {
                            Console.WriteLine("[ERR] input is not a number");
                        }
                        continue;    
                    case "3":
                        displayBalance(account.accountBalance);
                        continue;
                    case "0":
                        Environment.Exit(0);
                            break;
                    default:
                            Console.WriteLine("[ERR] invalid menu ..!!");
                            continue;
                  
                }
            }
            

        }
    }
}
